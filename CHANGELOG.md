# Changelog

## [1.0.2] - 2023-09-25

### Changed

- Requirements version update


## [1.0.1] - 2023-09-21

### Changed

- The module now uses the new version of CommonOcean IO (2023.1)
- The package is no longer compatible with Python 3.7
